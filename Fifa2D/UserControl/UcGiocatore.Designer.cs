﻿namespace Fifa2D
{
    partial class UcGiocatore
    {
        /// <summary> 
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione componenti

        /// <summary> 
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare 
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.pbColore2 = new System.Windows.Forms.PictureBox();
            this.pbColore1 = new System.Windows.Forms.PictureBox();
            this.pbCentro = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbColore2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbColore1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCentro)).BeginInit();
            this.SuspendLayout();
            // 
            // pbColore2
            // 
            this.pbColore2.BackColor = System.Drawing.Color.White;
            this.pbColore2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pbColore2.Location = new System.Drawing.Point(0, 0);
            this.pbColore2.Name = "pbColore2";
            this.pbColore2.Size = new System.Drawing.Size(50, 50);
            this.pbColore2.TabIndex = 0;
            this.pbColore2.TabStop = false;
            // 
            // pbColore1
            // 
            this.pbColore1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.pbColore1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pbColore1.Location = new System.Drawing.Point(5, 5);
            this.pbColore1.Name = "pbColore1";
            this.pbColore1.Size = new System.Drawing.Size(40, 40);
            this.pbColore1.TabIndex = 1;
            this.pbColore1.TabStop = false;
            // 
            // pbCentro
            // 
            this.pbCentro.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.pbCentro.Location = new System.Drawing.Point(18, 18);
            this.pbCentro.Name = "pbCentro";
            this.pbCentro.Size = new System.Drawing.Size(15, 15);
            this.pbCentro.TabIndex = 2;
            this.pbCentro.TabStop = false;
            // 
            // UcGiocatore
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.pbCentro);
            this.Controls.Add(this.pbColore1);
            this.Controls.Add(this.pbColore2);
            this.Name = "UcGiocatore";
            this.Size = new System.Drawing.Size(50, 50);
            this.Load += new System.EventHandler(this.UcGiocatore_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbColore2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbColore1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCentro)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pbColore2;
        private System.Windows.Forms.PictureBox pbColore1;
        private System.Windows.Forms.PictureBox pbCentro;
    }
}
