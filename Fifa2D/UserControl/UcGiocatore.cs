﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fifa2D
{
    public partial class UcGiocatore : UserControl
    {
        public Color _coloreCentro=Color.Black;
        public Color _colore1=Color.Black;
        public Color _colore2=Color.White;
        public int _squadra = 0;

        public UcGiocatore(Color Colore1, Color Colore2, string Nome, int Squadra)
        {
            InitializeComponent();
            _colore1 = Colore1;
            _colore2 = Colore2;
            _squadra = Squadra;
        }

        public void cambiaColore(Color colore)
        {
            pbCentro.BackColor = colore;
        }

        private void UcGiocatore_Load(object sender, EventArgs e)
        {
            this.Size = new Size(50, 50);
            pbColore1.BackColor = _colore1;
            pbColore2.BackColor = _colore2;
            pbCentro.BackColor = _colore1;
        }
    }
}
