﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fifa2D
{
    public class ClsCalciatoreDL
    {
        // Costanti e enumeratori

        public enum eStato
        {
            controllato,
            automatico,
            sospeso
        }

        public enum eDirezione
        {
            nord,
            nordEst,
            est,
            sudEst,
            sud,
            sudOvest,
            ovest,
            nordOvest
        }

        public enum eRuolo
        {
            attaccante,
            difensore,
            centroCampista,
            portiere
        }

        // Attributi
        private string id;
        private string nome;
        private string cognome;
        private eStato statoCalciatore;
        private eRuolo ruolo;
        private string idSquadra;  // Forse c'è un modo migliore di dirlo

        public UcGiocatore ucGiocatore;

        // Proprietà
        public string Id
        {
            get => id;
            set
            {
                if (!string.IsNullOrEmpty(value))
                    id = value;
                else
                    throw new ArgumentException("L'ID non può essere vuoto.");
            }
        }

        public string Nome
        {
            get => nome;
            set
            {
                if (!string.IsNullOrEmpty(value))
                    nome = value;
                else
                    throw new ArgumentException("Il nome non può essere vuoto.");
            }
        }

        public string Cognome
        {
            get => cognome;
            set
            {
                if (!string.IsNullOrEmpty(value))
                    cognome = value;
                else
                    throw new ArgumentException("Il cognome non può essere vuoto.");
            }
        }
        public eStato StatoCalciatore { get => statoCalciatore; set => statoCalciatore = value; }
        public eRuolo Ruolo { get => ruolo; set => ruolo = value; }
        public string IdSquadra { get => idSquadra; set => idSquadra = value; }

        // Costruttori
        public ClsCalciatoreDL()
        {
        }

    }
}
