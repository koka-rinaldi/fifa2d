﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fifa2D
{
    public class ClsGoal
    {
        enum eTempo
        {
            PRIMO,
            SECONDO,
            PRIMOSUPPLEMENTARE,
            SECONDOSUPPLEMENTARE
        }
        // Attributi
        private string iD;
        private int minuto;
        private bool autogol;
        private eTempo tempo;
        private string iDCalciatore;
        private string iDPartita;

        // Proprietà
        public string ID
        {
            get => iD;
            set
            {
                if (!string.IsNullOrEmpty(value))
                    iD = value;
                else
                    throw new ArgumentException("L'ID non può essere vuoto.");
            }
        }
        // DA GESTIRE MEGLIO AGGIUNGENDO attributo TEMPO ( 1 o 2) e RECUPERO
        public int Minuto
        {
            get => minuto;
            set
            {
                if (value >= 1 && value <= 90)
                    minuto = value;
                else
                    throw new ArgumentOutOfRangeException("Il minuto deve essere compreso tra 1 e 90.");
            }
        }
        // Se è true, controllo che il calciatore non appartenga alla squadra che ha segnato. 
        public bool Autogol { get => autogol; set => autogol = value; }

        public string IDCalciatore { get => iDCalciatore; set => iDCalciatore = value; }
        public string IDPartita { get => iDPartita; set => iDPartita = value; }

        // Costruttori
        public ClsGoal(int minuto, bool autogol, int minutiDiRecupero)
        {
            Minuto = minuto;
            Autogol = autogol; // Se il calciatore non appartiene alla squadra che ha segnato, imposto true.

        }

        public string RicercaSquadraGoal()
        {
            string _stringa = string.Empty;

            return _stringa;
        }
    }
}
