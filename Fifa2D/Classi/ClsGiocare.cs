﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fifa2D
{
    public class ClsGiocare
    {
        // Attributi
        private int iDGiocare;  // iD giocare formato da iDPartita e username in modo da poterne creare più di una
        private string iDPartita;
        private string iDsquadra;
        private string usernameUtente;

        // Proprietà
        public int IDGiocare { get => iDGiocare; set => iDGiocare = value; }
        public string IDPartita { get => iDPartita; set => iDPartita = value; }
        public string UsernameUtente { get => usernameUtente; set => usernameUtente = value; }
        public string IDsquadra { get => iDsquadra; set => iDsquadra = value; }

        // Costruttori
        public ClsGiocare()
        {

        }
    }
}
