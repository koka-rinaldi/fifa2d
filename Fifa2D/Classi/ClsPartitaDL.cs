﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fifa2D
{
    public class ClsPartitaDL
    {
        // Costanti e enumeratori

        public enum eStato
        {
            inCorso,
            inPausa,
            conclusa,
            sospesa
        }

        // Attributi
        private string iD;
        private eStato stato;
        private short punteggioSquadra1;
        private short punteggioSquadra2;
        private int durataTempo; //minuti reali
        private string utente1;
        private string utente2;
        private string squadra1;
        private string squadra2;

        // Proprietà
        public string ID
        {
            get => iD;
            set
            {
                if (!string.IsNullOrEmpty(value))
                    iD = value;
                else
                    throw new ArgumentException("L'ID non può essere vuoto.");
            }
        }

        public eStato Stato { get => stato; set => stato = value; }

        public short PunteggioSquadra1
        {
            get => punteggioSquadra1;
            set
            {
                if (value >= 0)
                    punteggioSquadra1 = value;
                else
                    throw new ArgumentOutOfRangeException("Il punteggio non può essere negativo.");
            }
        }

        public short PunteggioSquadra2
        {
            get => punteggioSquadra2;
            set
            {
                if (value >= 0)
                    punteggioSquadra2 = value;
                else
                    throw new ArgumentOutOfRangeException("Il punteggio non può essere negativo.");
            }
        }

        public int DurataTempo
        {
            get => durataTempo;
            set
            {
                if (value >= 3 && value <= 8)
                    durataTempo = value;
                else
                    throw new ArgumentOutOfRangeException("La durata del tempo deve essere compresa tra 3 e 8 minuti.");
            }
        }


        public string Utente1 { get => utente1; set => utente1 = value; }
        public string Utente2 { get => utente2; set => utente2 = value; }
        public string Squadra1 { get => squadra1; set => squadra1 = value; }
        public string Squadra2 { get => squadra2; set => squadra2 = value; }

        // Costruttori
        public ClsPartitaDL()
        {

        }

        public ClsPartitaDL(string squadra1, string squadra2)
        {
            Squadra1 = squadra1;
            Squadra2 = squadra2;
        }
        
    }
}
