﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fifa2D
{
    public class ClsSquadraDL
    {

        // Attributi
        private string idSquadra;
        private string nome;
        private ConsoleColor colore1;
        private ConsoleColor colore2;

        // Proprietà
        public string IdSquadra { get => idSquadra; set => idSquadra = value; }
        public string Nome
        {
            get => nome;
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ArgumentException("Il campo Nome non può essere vuoto.");
                }
                nome = value;
            }
        }

        public ConsoleColor Colore1 { get => colore1; set => colore1 = value; }
        public ConsoleColor Colore2 { get => colore2; set => colore2 = value; }

        // Costruttori
        public ClsSquadraDL()
        {
        }
    }
}
