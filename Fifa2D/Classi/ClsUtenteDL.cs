﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fifa2D
{
    public class ClsUtenteDL
    {
        // Attributi
        private string username;
        private string nome;
        private string cognome;
        private string idSquadraControllata;

        // Proprietà
        public string Username
        {
            get => username;
            set
            {
                if (!string.IsNullOrEmpty(value))
                    username = value;
                else
                    throw new ArgumentException("Lo username non può essere vuoto.");
            }
        }

        public string Nome
        {
            get => nome;
            set
            {
                if (!string.IsNullOrEmpty(value))
                    nome = value;
                else
                    throw new ArgumentException("Il nome non può essere vuoto.");
            }
        }

        public string Cognome
        {
            get => cognome;
            set
            {
                if (!string.IsNullOrEmpty(value))
                    cognome = value;
                else
                    throw new ArgumentException("Il cognome non può essere vuoto.");
            }
        }

        public string IdSquadraControllata { get => idSquadraControllata; set => idSquadraControllata = value; }

        // Costruttori
        public ClsUtenteDL()
        {

        }
    }
}
