﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Fifa2D
{
    public partial class FrmPartita : Form
    {
        private bool isKeyPressedC, isKeyPressedW, isKeyPressedS, isKeyPressedA, isKeyPressedD, isKeyPressedShift;
        private bool isKeyPressedL, isKeyPressedUP, isKeyPressedDOWN, isKeyPressedSX, isKeyPressedDX, isKeyPressedSpace;
        public short _squadraTiro=1, _movimentoPalla=0;

        Program.eStatoPalla _statoPalla = Program.eStatoPalla.ferma, _ExStatoPalla = Program.eStatoPalla.aSquadra1;
        enum eDirezione
        {
            EST,
            NORD,
            SUD,
            OVEST,
            NE,
            NO,
            SE,
            SO
        }
        eDirezione _direzione = eDirezione.EST;

        const short MOVPALLAPASS=4;
        const short MOVPALLATIRO=8;
        const short XPOR1 = 15;
        const short XPOR2 = 1013;
        const short YPOR = 262;
        const short XDC = 120;
        const short XDC2 = 901;
        const short XC = 261;
        const short XC2 = 760;
        const short YC = 148;
        const short XATT = 411;
        const short XATT2 = 610;


        UcGiocatore ucPOR1, ucDC1, ucCS1, ucCD1, ucATT1, ucPOR2, ucDC2, ucCS2, ucCD2, ucATT2;
        UcGiocatore ucGiocatoreConPalla, ucGiocatore1attivo, ucGiocatore2attivo;
        
        bool _partitaincorso = false;

        public int _risultato1 = 0, _risultato2 = 0;
        int _secondi = 0, _minuti = 0;


        private void timerPassaggio_Tick(object sender, EventArgs e)
        {
            if (_statoPalla == Program.eStatoPalla.movimento && _partitaincorso)
            {
                lancio(_squadraTiro,_movimentoPalla);
            }
        }

        private void FrmPartita_KeyUp(object sender, KeyEventArgs e)
        {
            if (_partitaincorso)
            {
                if (e.KeyCode == Keys.ShiftKey)
                {
                    isKeyPressedShift = false;
                }
                if (e.KeyCode == Keys.Space)
                {
                    isKeyPressedSpace = false;
                }

                if (e.KeyCode == Keys.C)
                {
                    isKeyPressedC = false;
                }
                if (e.KeyCode == Keys.W)
                {
                    isKeyPressedW = false;
                }
                if (e.KeyCode == Keys.A)
                {
                    isKeyPressedA = false;
                }
                if (e.KeyCode == Keys.S)
                {
                    isKeyPressedS = false;
                }
                if (e.KeyCode == Keys.D)
                {
                    isKeyPressedD = false;
                }

                if (e.KeyCode == Keys.L)
                {
                    isKeyPressedL = false;
                }
                if (e.KeyCode == Keys.Up)
                {
                    isKeyPressedUP = false;
                }
                if (e.KeyCode == Keys.Left)
                {
                    isKeyPressedSX = false;
                }
                if (e.KeyCode == Keys.Down)
                {
                    isKeyPressedDOWN = false;
                }
                if (e.KeyCode == Keys.Right)
                {
                    isKeyPressedDX = false;
                }
            }
        }

        private void lancio(int squadra,short movpalla)
        {
            
            if (_direzione == eDirezione.EST)
                pbPalla.Left += movpalla;
            else if (_direzione == eDirezione.OVEST)
                pbPalla.Left -= movpalla;
            else if (_direzione == eDirezione.NORD)
                pbPalla.Top -= movpalla;
            else if (_direzione == eDirezione.SUD)
                pbPalla.Top += movpalla;

            else if (_direzione == eDirezione.SE)
            {
                pbPalla.Top += movpalla;
                pbPalla.Left += movpalla;
            }
            else if (_direzione == eDirezione.NE)
            {
                pbPalla.Top -= movpalla;
                pbPalla.Left += movpalla;
            }
            else if (_direzione == eDirezione.SO)
            {
                pbPalla.Top += movpalla;
                pbPalla.Left -= movpalla;
            }
            else if (_direzione == eDirezione.NO)
            {
                pbPalla.Top -= movpalla;
                pbPalla.Left -= movpalla;
            }
            
        }

        Program.sSquadra _squadra1, _squadra2;

        public FrmPartita(Program.sSquadra Squadra1, Program.sSquadra Squadra2)
        {
            InitializeComponent();
            _squadra1 = Squadra1;
            _squadra2 = Squadra2;
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (_partitaincorso)
            {
                if (keyData == Keys.Up)
                {
                    isKeyPressedUP = true;
                }
                if (keyData == Keys.Left)
                {
                    isKeyPressedSX = true;
                }
                if (keyData == Keys.Down)
                {
                    isKeyPressedDOWN = true;
                }
                if (keyData == Keys.Right)
                {
                    isKeyPressedDX = true;
                }


            }
            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void FrmPartita_Load(object sender, EventArgs e)
        {
            pnlCampo.Size = new Size(1080, 580);
            timerPassaggio.Interval = 30;
            
            timerTempo.Interval = Program._minutiPartita * 60 * 1000;
            timerSecondi.Interval = 1000;
            pbSquadra1.BackgroundImage = (Image)Properties.Resources.ResourceManager.GetObject(_squadra1.Nome);
            pbSquadra2.BackgroundImage = (Image)Properties.Resources.ResourceManager.GetObject(_squadra2.Nome);

            definisciFormazione(1);
            
            timerMovimento = new Timer();
            timerMovimento.Interval = 7; 
            timerMovimento.Tick += timerMovimento_Tick;

            ucGiocatore1attivo = ucATT1;

            ucGiocatore2attivo = ucATT2;
        }

        
        private void definisciFormazione(int squadraConPalla)
        {
            ucPOR1 = new UcGiocatore(_squadra1.Colore1, _squadra1.Colore2, _squadra1.POR,1);
            ucPOR1.Tag = _squadra1.POR;
            pnlCampo.Controls.Add(ucPOR1);

            ucDC1 = new UcGiocatore(_squadra1.Colore1, _squadra1.Colore2, _squadra1.DC, 1);
            ucDC1.Tag = _squadra1.DC;
            pnlCampo.Controls.Add(ucDC1);

            ucCS1 = new UcGiocatore(_squadra1.Colore1, _squadra1.Colore2, _squadra1.CS, 1);
            ucCS1.Tag = _squadra1.CS;
            pnlCampo.Controls.Add(ucCS1);

            ucCD1 = new UcGiocatore(_squadra1.Colore1, _squadra1.Colore2, _squadra1.CD, 1);
            ucCD1.Tag = _squadra1.CD;
            pnlCampo.Controls.Add(ucCD1);

            ucATT1 = new UcGiocatore(_squadra1.Colore1, _squadra1.Colore2, _squadra1.ATT, 1);
            ucATT1.Tag = _squadra1.ATT;
            pnlCampo.Controls.Add(ucATT1);

            ucPOR2 = new UcGiocatore(_squadra2.Colore1, _squadra2.Colore2, _squadra2.POR, 2);
            ucPOR2.Tag = _squadra2.POR;
            pnlCampo.Controls.Add(ucPOR2);

            ucDC2 = new UcGiocatore(_squadra2.Colore1, _squadra2.Colore2, _squadra2.DC, 2);
            ucDC2.Tag = _squadra2.DC;
            pnlCampo.Controls.Add(ucDC2);

            ucCS2 = new UcGiocatore(_squadra2.Colore1, _squadra2.Colore2, _squadra2.CS, 2);
            ucCS2.Tag = _squadra2.CS;
            pnlCampo.Controls.Add(ucCS2);

            ucCD2 = new UcGiocatore(_squadra2.Colore1, _squadra2.Colore2, _squadra2.CD, 2);
            ucCD2.Tag = _squadra2.CD;
            pnlCampo.Controls.Add(ucCD2);

            ucATT2 = new UcGiocatore(_squadra2.Colore1, _squadra2.Colore2, _squadra2.ATT, 2);
            ucATT2.Tag = _squadra2.ATT;

            ucATT2.Tag = _squadra2.ATT;
            pnlCampo.Controls.Add(ucATT2);
            pbRigaCentrocampo.SendToBack();


            posizionaGiocatori(squadraConPalla);
        }

        public UcGiocatore DeterminaGiocatoreConPallone()
        {
            foreach (Control uc in pnlCampo.Controls)
            {
                if (uc is UcGiocatore giocatore)
                {
                    if (pbPalla.Bounds.IntersectsWith(giocatore.Bounds))
                    {
                        return giocatore; 
                    }
                }
            }
            return null;
        }

        private void btAvvia_Click(object sender, EventArgs e)
        {
            _partitaincorso = true;
            timerTempo.Start();
            timerSecondi.Start();
            
            timerMovimento.Start();
            btAvvia.Visible = false;           

            ucGiocatoreConPalla = ucATT1;
            _statoPalla = Program.eStatoPalla.aSquadra1;
        }
        private void timerMovimento_Tick(object sender, EventArgs e)
        {
            if (_partitaincorso)
            {
                if (isKeyPressedW)
                {
                    ucGiocatore1attivo.Top -= 2;
                }
                if (isKeyPressedD)
                {
                    ucGiocatore1attivo.Left += 2;
                }
                if (isKeyPressedS)
                {
                    ucGiocatore1attivo.Top += 2;
                }
                if (isKeyPressedA)
                {
                    ucGiocatore1attivo.Left -= 2;
                }


                if (isKeyPressedUP)
                {
                    ucGiocatore2attivo.Top -= 2;
                }
                if (isKeyPressedDX)
                {
                    ucGiocatore2attivo.Left += 2;
                }
                if (isKeyPressedDOWN)
                {
                    ucGiocatore2attivo.Top += 2;
                }
                if (isKeyPressedSX)
                {
                    ucGiocatore2attivo.Left -= 2;
                }


                if (_statoPalla == Program.eStatoPalla.aSquadra1)
                {
                    pbPalla.Location = new Point(ucGiocatoreConPalla.Location.X+ucGiocatoreConPalla.Width - 20, ucGiocatoreConPalla.Location.Y + 5);
                }

                else if (_statoPalla == Program.eStatoPalla.aSquadra2)
                {
                    pbPalla.Location = new Point(ucGiocatoreConPalla.Location.X-20, ucGiocatoreConPalla.Location.Y + 5);
                }

                foreach (Control cc in pnlCampo.Controls)
                {
                    if (cc is PictureBox && pbPalla.Bounds.IntersectsWith(cc.Bounds))
                    {
                        if (cc.Tag != null && cc.Tag.ToString() == "porta2")
                        {
                            goal(1);
                        }
                        else if (cc.Tag != null && cc.Tag.ToString() == "porta1")
                        {
                                goal(2);
                        }
                       
                        else if (cc.Tag.ToString() == "lineaLaterale")
                        {
                            // La palla è uscita
                            // Riparti con la squadra avversaria che ha palla a metà campo
                            if ( _statoPalla == Program.eStatoPalla.aSquadra1 || _ExStatoPalla == Program.eStatoPalla.aSquadra1)
                            {                                
                                posizionaGiocatori(2);
                            }
                            else if (_statoPalla == Program.eStatoPalla.aSquadra2 || _ExStatoPalla == Program.eStatoPalla.aSquadra2)
                            {
                                posizionaGiocatori(1);
                            }
                            
                        }
                        
                    }
                }

                foreach (UcGiocatore uc in pnlCampo.Controls.OfType<UcGiocatore>())
                {
                    uc.cambiaColore(uc._colore1);                  
                }
                if (_statoPalla == Program.eStatoPalla.aSquadra1)
                {
                    ucPOR2.BringToFront();
                    ucDC2.BringToFront();
                    ucCD2.BringToFront();
                    ucCS2.BringToFront();
                    ucATT2.BringToFront();
                }
                else if (_statoPalla == Program.eStatoPalla.aSquadra2)
                {
                    ucPOR1.BringToFront();
                    ucDC1.BringToFront();
                    ucCD1.BringToFront();
                    ucCS1.BringToFront();
                    ucATT1.BringToFront();
                }
                pbPalla.BringToFront();

                ucGiocatore1attivo.cambiaColore(Color.Gold);
                ucGiocatore2attivo.cambiaColore(Color.Gold);

                ucGiocatoreConPalla = DeterminaGiocatoreConPallone();
                if (ucGiocatoreConPalla != null)
                {
                    _statoPalla = Program.eStatoPalla.movimento;
                    if (ucGiocatoreConPalla._squadra == 1)
                    {
                        ucGiocatore1attivo = ucGiocatoreConPalla;
                       _statoPalla = Program.eStatoPalla.aSquadra1;
                    }
                    else if (ucGiocatoreConPalla._squadra == 2)
                    {
                        ucGiocatore2attivo = ucGiocatoreConPalla;
                        _statoPalla = Program.eStatoPalla.aSquadra2;
                    }

                    // FUNZIONE PER MUOVERE INTELLIGENTEMENTE I GIOCATORI.
                    //muoviGiocatori();
                }
            }
        }

        private void goal(int squadra) {

            string nomeGiocatore = "";
            string squadraGoal="";
            if (squadra == 1)
            {
                _risultato1++;
                squadraGoal = _squadra1.Nome;
                posizionaGiocatori(2);
                nomeGiocatore = ucGiocatore1attivo.Tag.ToString();
                isKeyPressedW = false;
                isKeyPressedA = false;
                isKeyPressedS = false;
                isKeyPressedD = false;
            }
            else if (squadra == 2)
            {
                _risultato2++;
                squadraGoal = _squadra2.Nome;
                posizionaGiocatori(1);
                nomeGiocatore = ucGiocatore2attivo.Tag.ToString();
            }
            MessageBox.Show("Ha segnato " + nomeGiocatore + " della squadra " + squadraGoal);
        }

        private void posizionaGiocatori(int squadraConPalla)
        {
            pbPalla.Location = new Point(pnlCampo.Width / 2 - pbPalla.Width / 2, pnlCampo.Height / 2 - pbPalla.Height / 2);            ucPOR1.Location = new Point(XPOR1, YPOR);
            ucDC1.Location = new Point(XDC, YPOR);
            ucCS1.Location = new Point(XC, YC);
            ucCD1.Location = new Point(XC, pnlCampo.Height - YC - pbPalla.Height);
            if (squadraConPalla == 1)
            {
                ucATT1.Location = new Point(XATT + 89, YPOR);
                ucGiocatoreConPalla = ucATT1;
            }
            else
                ucATT1.Location = new Point(XATT, YPOR);
            ucPOR2.Location = new Point(XPOR2, YPOR);
            ucDC2.Location = new Point(XDC2, YPOR);
            ucCS2.Location = new Point(XC2, YC);
            ucCD2.Location = new Point(XC2, pnlCampo.Height - YC - pbPalla.Height);
            if (squadraConPalla == 2)
            {
                ucATT2.Location = new Point(XATT2 - 89, YPOR);
                ucGiocatoreConPalla = ucATT2;
            }
            else
                ucATT2.Location = new Point(XATT2, YPOR);

        }
        private void FrmPartita_KeyDown(object sender, KeyEventArgs e)
        {
            if (_partitaincorso)
            {
                if (e.KeyCode == Keys.ShiftKey)
                {
                    isKeyPressedShift = true;
                }
                if (e.KeyCode == Keys.Space)
                {
                    isKeyPressedSpace = true;
                }
                if (e.KeyCode == Keys.C)
                {
                    isKeyPressedC = true;
                }
                if (e.KeyCode == Keys.W)
                {
                    isKeyPressedW = true;
                }
                if (e.KeyCode == Keys.A)
                {
                    isKeyPressedA = true;
                }
                if (e.KeyCode == Keys.S)
                {
                    isKeyPressedS = true;
                }
                if (e.KeyCode == Keys.D)
                {
                    isKeyPressedD = true;
                }
                if (e.KeyCode == Keys.L)
                {
                    isKeyPressedL = true;
                }
                

                if (e.KeyCode == Keys.Escape)
                {
                    timerTempo.Stop();
                    timerSecondi.Stop();
                    timerMovimento.Stop();
                    _partitaincorso = false;
                    FrmPausa frmPausa = new FrmPausa();
                    DialogResult dr = frmPausa.ShowDialog(this);

                    if (dr == DialogResult.OK)
                    {
                        _partitaincorso = true;
                        timerTempo.Start();
                        timerSecondi.Start();
                        timerMovimento.Start();
                    }
                    else if (dr == DialogResult.Abort)
                    {
                        this.DialogResult = DialogResult.Abort;
                        this.Close();
                    }
                }

                if (e.KeyCode == Keys.Z && _statoPalla!=Program.eStatoPalla.aSquadra1)
                {
                    CambiaGiocatoreAttivo(1);
                }

                if (e.KeyCode == Keys.Oem7 && _statoPalla != Program.eStatoPalla.aSquadra2)
                {
                    CambiaGiocatoreAttivo(2);
                }

                if (isKeyPressedC)
                {
                    if (_statoPalla == Program.eStatoPalla.aSquadra1)
                    {
                        TrovaDirezione();
                        _movimentoPalla = MOVPALLAPASS;
                        _ExStatoPalla = _statoPalla;
                        timerPassaggio.Start();
                        _statoPalla = Program.eStatoPalla.movimento;                       
                    }
                }
                if (isKeyPressedL)
                {
                    if (_statoPalla == Program.eStatoPalla.aSquadra2)
                    {
                        TrovaDirezione();
                        _movimentoPalla = MOVPALLAPASS;
                        _ExStatoPalla = _statoPalla;
                        timerPassaggio.Start();
                        _statoPalla = Program.eStatoPalla.movimento;
                    }
                }

                if (isKeyPressedShift)
                {
                    if (_statoPalla == Program.eStatoPalla.aSquadra1)
                    {
                        TrovaDirezione();
                        _squadraTiro = 1;
                        _movimentoPalla = MOVPALLATIRO;
                        _ExStatoPalla = _statoPalla;
                        timerPassaggio.Start();
                        _statoPalla = Program.eStatoPalla.movimento;
                    }
                }
                if (isKeyPressedSpace)
                {
                    if (_statoPalla == Program.eStatoPalla.aSquadra2)
                    {
                        TrovaDirezione();
                        _squadraTiro = 2;
                        _movimentoPalla = MOVPALLATIRO;
                        _ExStatoPalla = _statoPalla;
                        timerPassaggio.Start();
                        _statoPalla = Program.eStatoPalla.movimento;
                    }
                }
            }         
        }

        private void TrovaDirezione()
        {
            if (_statoPalla == Program.eStatoPalla.aSquadra1)
            {
                if (isKeyPressedD)
                {
                    _direzione = eDirezione.EST;
                    pbPalla.Location = new Point(ucGiocatoreConPalla.Location.X + 55, ucGiocatoreConPalla.Location.Y + 5);
                }
                else if (isKeyPressedW)
                {
                    _direzione = eDirezione.NORD;
                    pbPalla.Location = new Point(ucGiocatoreConPalla.Location.X + 5, ucGiocatoreConPalla.Location.Y - 55);
                }
                else if (isKeyPressedA)
                {
                    _direzione = eDirezione.OVEST;
                    pbPalla.Location = new Point(ucGiocatoreConPalla.Location.X - 55, ucGiocatoreConPalla.Location.Y + 5);
                }
                else if (isKeyPressedS)
                {
                    _direzione = eDirezione.SUD;
                    pbPalla.Location = new Point(ucGiocatoreConPalla.Location.X + 5, ucGiocatoreConPalla.Location.Y + 55);
                }
                else
                {
                    _direzione = eDirezione.EST;
                    pbPalla.Location = new Point(ucGiocatoreConPalla.Location.X + 55, ucGiocatoreConPalla.Location.Y + 5);
                }
            }

            if (_statoPalla == Program.eStatoPalla.aSquadra2)
            {
                if (isKeyPressedSX)
                {
                    _direzione = eDirezione.OVEST;
                    pbPalla.Location = new Point(ucGiocatoreConPalla.Location.X - 55, ucGiocatoreConPalla.Location.Y + 5);
                }
                else if (isKeyPressedUP)
                {
                    _direzione = eDirezione.NORD;
                    pbPalla.Location = new Point(ucGiocatoreConPalla.Location.X + 5, ucGiocatoreConPalla.Location.Y - 55);
                }
                else if (isKeyPressedDX)
                {
                    _direzione = eDirezione.EST;
                    pbPalla.Location = new Point(ucGiocatoreConPalla.Location.X + 55, ucGiocatoreConPalla.Location.Y + 5);
                }
                else if (isKeyPressedDOWN)
                {
                    _direzione = eDirezione.SUD;
                    pbPalla.Location = new Point(ucGiocatoreConPalla.Location.X + 5, ucGiocatoreConPalla.Location.Y + 55);
                }
                else
                {
                    _direzione = eDirezione.OVEST;
                    pbPalla.Location = new Point(ucGiocatoreConPalla.Location.X - 55, ucGiocatoreConPalla.Location.Y + 5);
                }
            }
        }

        private void CambiaGiocatoreAttivo(int squadraSenzaPalla)
        {
            UcGiocatore giocatorePiùVicino = null;
            UcGiocatore secondoGiocatorePiùVicino = null;
            double distanzaMinima = double.MaxValue;
            double secondaDistanzaMinima = double.MaxValue;

            foreach (Control controllo in pnlCampo.Controls)
            {
                if (controllo is UcGiocatore giocatore && giocatore._squadra == squadraSenzaPalla)
                {
                    double distanza = CalcolaDistanza(giocatore.Location, pbPalla.Location);
                    if (distanza < distanzaMinima)
                    {
                        secondaDistanzaMinima = distanzaMinima;
                        distanzaMinima = distanza;
                        secondoGiocatorePiùVicino = giocatorePiùVicino;
                        giocatorePiùVicino = giocatore;
                    }
                    else if (distanza < secondaDistanzaMinima)
                    {
                        secondaDistanzaMinima = distanza;
                        secondoGiocatorePiùVicino = giocatore;
                    }
                }
            }

            if (giocatorePiùVicino != null && secondoGiocatorePiùVicino != null)
            {
                if (squadraSenzaPalla == 1)
                {
                    if (ucGiocatore1attivo == giocatorePiùVicino)
                    {
                        ucGiocatore1attivo = secondoGiocatorePiùVicino;
                    }
                    else
                    {
                        ucGiocatore1attivo = giocatorePiùVicino;
                    }
                }
                else if (squadraSenzaPalla == 2)
                {
                    if (ucGiocatore2attivo == giocatorePiùVicino)
                    {
                        ucGiocatore2attivo = secondoGiocatorePiùVicino;
                    }
                    else
                    {
                        ucGiocatore2attivo = giocatorePiùVicino;
                    }
                }
            }
        }
        private void muoviGiocatori()
        {
            foreach (UcGiocatore uc in pnlCampo.Controls.OfType<UcGiocatore>())
            {
                if (uc != ucGiocatore1attivo && uc != ucGiocatore2attivo)
                {
                    if (uc._squadra == 1)
                    {
                        // Movimento giocatori squadra 1 in base al ruolo e alla posizione della palla
                        if (_statoPalla == Program.eStatoPalla.aSquadra1)
                        {
                            string nomeGiocatore = uc.Tag.ToString();

                            if (nomeGiocatore == _squadra1.POR)
                            {
                                // Ruolo portiere squadra 1
                                // Implementazione del movimento intelligente per il portiere
                            }
                            else if (nomeGiocatore == _squadra1.DC)
                            {
                                // Ruolo difensore centrale squadra 1
                                // Implementazione del movimento intelligente per il difensore centrale
                            }
                            else if (nomeGiocatore == _squadra1.CS)
                            {
                                // Ruolo centrocampista sinistro squadra 1
                                // Implementazione del movimento intelligente per il centrocampista sinistro
                            }
                            else if (nomeGiocatore == _squadra1.CD)
                            {
                                // Ruolo centrocampista destro squadra 1
                                // Implementazione del movimento intelligente per il centrocampista destro
                            }
                            else if (nomeGiocatore == _squadra1.ATT)
                            {
                                // Ruolo attaccante squadra 1
                                // Implementazione del movimento intelligente per l'attaccante
                            }
                        }
                    }
                    else if (uc._squadra == 2)
                    {
                        // Movimento giocatori squadra 2 in base al ruolo e alla posizione della palla
                        if (_statoPalla == Program.eStatoPalla.aSquadra2)
                        {
                            string nomeGiocatore = uc.Tag.ToString();

                            if (nomeGiocatore == _squadra2.POR)
                            {
                                // Ruolo portiere squadra 2
                                // Implementazione del movimento intelligente per il portiere
                            }
                            else if (nomeGiocatore == _squadra2.DC)
                            {
                                // Ruolo difensore centrale squadra 2
                                // Implementazione del movimento intelligente per il difensore centrale
                            }
                            else if (nomeGiocatore == _squadra2.CS)
                            {
                                // Ruolo centrocampista sinistro squadra 2
                                // Implementazione del movimento intelligente per il centrocampista sinistro
                            }
                            else if (nomeGiocatore == _squadra2.CD)
                            {
                                // Ruolo centrocampista destro squadra 2
                                // Implementazione del movimento intelligente per il centrocampista destro
                            }
                            else if (nomeGiocatore == _squadra2.ATT)
                            {
                                // Ruolo attaccante squadra 2
                                // Implementazione del movimento intelligente per l'attaccante
                            }
                        }
                    }
                }
            }
        }
        private double CalcolaDistanza(Point punto1, Point punto2)
        {
            int deltaX = punto2.X - punto1.X;
            int deltaY = punto2.Y - punto1.Y;
            return Math.Sqrt(deltaX * deltaX + deltaY * deltaY);
        }        
        private void timerTempo_Tick(object sender, EventArgs e)
        {
            _partitaincorso = false;
            timerTempo.Stop();
            timerSecondi.Stop();
        }

        private void timerSecondi_Tick(object sender, EventArgs e)
        {
            lblRisultato.Text = _risultato1 + "-" + _risultato2;
            _secondi += 1;
            if (_secondi >= 60)
            {
                _secondi = 0;
                _minuti += 1;
            }
            lblTimer.Text = _minuti.ToString()+"'"+_secondi.ToString()+"''";
        }

    }
}
