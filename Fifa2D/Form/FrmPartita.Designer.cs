﻿namespace Fifa2D
{
    partial class FrmPartita
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPartita));
            this.pnlCampo = new System.Windows.Forms.Panel();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btAvvia = new System.Windows.Forms.Button();
            this.pbPalla = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pbCornerDxUp = new System.Windows.Forms.PictureBox();
            this.pbCornerDxDw = new System.Windows.Forms.PictureBox();
            this.pbCornerSxUp = new System.Windows.Forms.PictureBox();
            this.pbCornerSxDw = new System.Windows.Forms.PictureBox();
            this.pbDwDx = new System.Windows.Forms.PictureBox();
            this.pbDwSx = new System.Windows.Forms.PictureBox();
            this.pbUpDx = new System.Windows.Forms.PictureBox();
            this.pbUpSx = new System.Windows.Forms.PictureBox();
            this.pbRigaCentrocampo = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblTimer = new System.Windows.Forms.Label();
            this.lblRisultato = new System.Windows.Forms.Label();
            this.pbSquadra2 = new System.Windows.Forms.PictureBox();
            this.pbSquadra1 = new System.Windows.Forms.PictureBox();
            this.timerTempo = new System.Windows.Forms.Timer(this.components);
            this.timerSecondi = new System.Windows.Forms.Timer(this.components);
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.lblGiocatore1 = new System.Windows.Forms.Label();
            this.lblGiocatore2 = new System.Windows.Forms.Label();
            this.timerMovimento = new System.Windows.Forms.Timer(this.components);
            this.timerPassaggio = new System.Windows.Forms.Timer(this.components);
            this.pnlCampo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPalla)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCornerDxUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCornerDxDw)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCornerSxUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCornerSxDw)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbDwDx)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbDwSx)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbUpDx)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbUpSx)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbRigaCentrocampo)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbSquadra2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbSquadra1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlCampo
            // 
            this.pnlCampo.BackColor = System.Drawing.Color.LimeGreen;
            this.pnlCampo.Controls.Add(this.pictureBox10);
            this.pnlCampo.Controls.Add(this.pictureBox9);
            this.pnlCampo.Controls.Add(this.pictureBox8);
            this.pnlCampo.Controls.Add(this.pictureBox2);
            this.pnlCampo.Controls.Add(this.pictureBox1);
            this.pnlCampo.Controls.Add(this.btAvvia);
            this.pnlCampo.Controls.Add(this.pbPalla);
            this.pnlCampo.Controls.Add(this.pictureBox6);
            this.pnlCampo.Controls.Add(this.pictureBox4);
            this.pnlCampo.Controls.Add(this.pbCornerDxUp);
            this.pnlCampo.Controls.Add(this.pbCornerDxDw);
            this.pnlCampo.Controls.Add(this.pbCornerSxUp);
            this.pnlCampo.Controls.Add(this.pbCornerSxDw);
            this.pnlCampo.Controls.Add(this.pbDwDx);
            this.pnlCampo.Controls.Add(this.pbDwSx);
            this.pnlCampo.Controls.Add(this.pbUpDx);
            this.pnlCampo.Controls.Add(this.pbUpSx);
            this.pnlCampo.Controls.Add(this.pbRigaCentrocampo);
            this.pnlCampo.Location = new System.Drawing.Point(88, 81);
            this.pnlCampo.Name = "pnlCampo";
            this.pnlCampo.Size = new System.Drawing.Size(1080, 580);
            this.pnlCampo.TabIndex = 0;
            this.pnlCampo.Tag = "campo";
            // 
            // pictureBox10
            // 
            this.pictureBox10.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pictureBox10.BackgroundImage = global::Fifa2D.Properties.Resources._palla;
            this.pictureBox10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox10.Location = new System.Drawing.Point(500, 271);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(40, 40);
            this.pictureBox10.TabIndex = 17;
            this.pictureBox10.TabStop = false;
            this.pictureBox10.Tag = "Palla";
            this.pictureBox10.Visible = false;
            // 
            // pictureBox9
            // 
            this.pictureBox9.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pictureBox9.BackgroundImage = global::Fifa2D.Properties.Resources._palla;
            this.pictureBox9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox9.Location = new System.Drawing.Point(261, 419);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(40, 40);
            this.pictureBox9.TabIndex = 16;
            this.pictureBox9.TabStop = false;
            this.pictureBox9.Tag = "Palla";
            this.pictureBox9.Visible = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pictureBox8.BackgroundImage = global::Fifa2D.Properties.Resources._palla;
            this.pictureBox8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox8.Location = new System.Drawing.Point(261, 148);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(40, 40);
            this.pictureBox8.TabIndex = 15;
            this.pictureBox8.TabStop = false;
            this.pictureBox8.Tag = "Palla";
            this.pictureBox8.Visible = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pictureBox2.BackgroundImage = global::Fifa2D.Properties.Resources._palla;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox2.Location = new System.Drawing.Point(15, 271);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(40, 40);
            this.pictureBox2.TabIndex = 14;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Tag = "Palla";
            this.pictureBox2.Visible = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pictureBox1.BackgroundImage = global::Fifa2D.Properties.Resources._palla;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox1.Location = new System.Drawing.Point(127, 271);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(40, 40);
            this.pictureBox1.TabIndex = 13;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Tag = "Palla";
            this.pictureBox1.Visible = false;
            // 
            // btAvvia
            // 
            this.btAvvia.BackColor = System.Drawing.Color.Transparent;
            this.btAvvia.BackgroundImage = global::Fifa2D.Properties.Resources._BtAvvia;
            this.btAvvia.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btAvvia.Cursor = System.Windows.Forms.Cursors.Cross;
            this.btAvvia.FlatAppearance.BorderSize = 0;
            this.btAvvia.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btAvvia.Location = new System.Drawing.Point(317, 322);
            this.btAvvia.Name = "btAvvia";
            this.btAvvia.Size = new System.Drawing.Size(446, 222);
            this.btAvvia.TabIndex = 12;
            this.btAvvia.UseVisualStyleBackColor = false;
            this.btAvvia.Click += new System.EventHandler(this.btAvvia_Click);
            // 
            // pbPalla
            // 
            this.pbPalla.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pbPalla.BackgroundImage = global::Fifa2D.Properties.Resources._palla;
            this.pbPalla.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pbPalla.Location = new System.Drawing.Point(779, 148);
            this.pbPalla.Name = "pbPalla";
            this.pbPalla.Size = new System.Drawing.Size(40, 40);
            this.pbPalla.TabIndex = 11;
            this.pbPalla.TabStop = false;
            this.pbPalla.Tag = "Palla";
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.pictureBox6.Location = new System.Drawing.Point(-43, 206);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(52, 168);
            this.pictureBox6.TabIndex = 10;
            this.pictureBox6.TabStop = false;
            this.pictureBox6.Tag = "porta1";
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.pictureBox4.Location = new System.Drawing.Point(1071, 206);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(54, 168);
            this.pictureBox4.TabIndex = 9;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.Tag = "porta2";
            // 
            // pbCornerDxUp
            // 
            this.pbCornerDxUp.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.pbCornerDxUp.Location = new System.Drawing.Point(1066, 0);
            this.pbCornerDxUp.Name = "pbCornerDxUp";
            this.pbCornerDxUp.Size = new System.Drawing.Size(14, 200);
            this.pbCornerDxUp.TabIndex = 7;
            this.pbCornerDxUp.TabStop = false;
            this.pbCornerDxUp.Tag = "lineaLaterale";
            // 
            // pbCornerDxDw
            // 
            this.pbCornerDxDw.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.pbCornerDxDw.Location = new System.Drawing.Point(1066, 380);
            this.pbCornerDxDw.Name = "pbCornerDxDw";
            this.pbCornerDxDw.Size = new System.Drawing.Size(14, 200);
            this.pbCornerDxDw.TabIndex = 6;
            this.pbCornerDxDw.TabStop = false;
            this.pbCornerDxDw.Tag = "lineaLaterale";
            // 
            // pbCornerSxUp
            // 
            this.pbCornerSxUp.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.pbCornerSxUp.Location = new System.Drawing.Point(0, 0);
            this.pbCornerSxUp.Name = "pbCornerSxUp";
            this.pbCornerSxUp.Size = new System.Drawing.Size(14, 200);
            this.pbCornerSxUp.TabIndex = 5;
            this.pbCornerSxUp.TabStop = false;
            this.pbCornerSxUp.Tag = "lineaLaterale";
            // 
            // pbCornerSxDw
            // 
            this.pbCornerSxDw.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.pbCornerSxDw.Location = new System.Drawing.Point(0, 380);
            this.pbCornerSxDw.Name = "pbCornerSxDw";
            this.pbCornerSxDw.Size = new System.Drawing.Size(14, 200);
            this.pbCornerSxDw.TabIndex = 4;
            this.pbCornerSxDw.TabStop = false;
            this.pbCornerSxDw.Tag = "lineaLaterale";
            // 
            // pbDwDx
            // 
            this.pbDwDx.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.pbDwDx.Location = new System.Drawing.Point(540, 566);
            this.pbDwDx.Name = "pbDwDx";
            this.pbDwDx.Size = new System.Drawing.Size(540, 14);
            this.pbDwDx.TabIndex = 3;
            this.pbDwDx.TabStop = false;
            this.pbDwDx.Tag = "lineaLaterale";
            // 
            // pbDwSx
            // 
            this.pbDwSx.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.pbDwSx.Location = new System.Drawing.Point(0, 566);
            this.pbDwSx.Name = "pbDwSx";
            this.pbDwSx.Size = new System.Drawing.Size(540, 14);
            this.pbDwSx.TabIndex = 2;
            this.pbDwSx.TabStop = false;
            this.pbDwSx.Tag = "lineaLaterale";
            // 
            // pbUpDx
            // 
            this.pbUpDx.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.pbUpDx.Location = new System.Drawing.Point(540, 0);
            this.pbUpDx.Name = "pbUpDx";
            this.pbUpDx.Size = new System.Drawing.Size(540, 14);
            this.pbUpDx.TabIndex = 1;
            this.pbUpDx.TabStop = false;
            this.pbUpDx.Tag = "lineaLaterale";
            // 
            // pbUpSx
            // 
            this.pbUpSx.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.pbUpSx.Location = new System.Drawing.Point(0, 0);
            this.pbUpSx.Name = "pbUpSx";
            this.pbUpSx.Size = new System.Drawing.Size(540, 14);
            this.pbUpSx.TabIndex = 0;
            this.pbUpSx.TabStop = false;
            this.pbUpSx.Tag = "lineaLaterale";
            // 
            // pbRigaCentrocampo
            // 
            this.pbRigaCentrocampo.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.pbRigaCentrocampo.Location = new System.Drawing.Point(533, 0);
            this.pbRigaCentrocampo.Name = "pbRigaCentrocampo";
            this.pbRigaCentrocampo.Size = new System.Drawing.Size(14, 580);
            this.pbRigaCentrocampo.TabIndex = 8;
            this.pbRigaCentrocampo.TabStop = false;
            this.pbRigaCentrocampo.Tag = "";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.HotTrack;
            this.panel1.Controls.Add(this.lblTimer);
            this.panel1.Controls.Add(this.lblRisultato);
            this.panel1.Controls.Add(this.pbSquadra2);
            this.panel1.Controls.Add(this.pbSquadra1);
            this.panel1.Location = new System.Drawing.Point(-1, -1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(418, 83);
            this.panel1.TabIndex = 1;
            // 
            // lblTimer
            // 
            this.lblTimer.AutoSize = true;
            this.lblTimer.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTimer.ForeColor = System.Drawing.Color.White;
            this.lblTimer.Location = new System.Drawing.Point(306, 20);
            this.lblTimer.Name = "lblTimer";
            this.lblTimer.Size = new System.Drawing.Size(101, 39);
            this.lblTimer.TabIndex = 3;
            this.lblTimer.Text = "0\'00\'\'";
            this.lblTimer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblRisultato
            // 
            this.lblRisultato.AutoSize = true;
            this.lblRisultato.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRisultato.ForeColor = System.Drawing.Color.White;
            this.lblRisultato.Location = new System.Drawing.Point(110, 20);
            this.lblRisultato.Name = "lblRisultato";
            this.lblRisultato.Size = new System.Drawing.Size(70, 39);
            this.lblRisultato.TabIndex = 2;
            this.lblRisultato.Text = "0-0";
            // 
            // pbSquadra2
            // 
            this.pbSquadra2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pbSquadra2.Location = new System.Drawing.Point(216, 10);
            this.pbSquadra2.Name = "pbSquadra2";
            this.pbSquadra2.Size = new System.Drawing.Size(68, 67);
            this.pbSquadra2.TabIndex = 1;
            this.pbSquadra2.TabStop = false;
            // 
            // pbSquadra1
            // 
            this.pbSquadra1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pbSquadra1.Location = new System.Drawing.Point(13, 10);
            this.pbSquadra1.Name = "pbSquadra1";
            this.pbSquadra1.Size = new System.Drawing.Size(68, 67);
            this.pbSquadra1.TabIndex = 0;
            this.pbSquadra1.TabStop = false;
            // 
            // timerTempo
            // 
            this.timerTempo.Tick += new System.EventHandler(this.timerTempo_Tick);
            // 
            // timerSecondi
            // 
            this.timerSecondi.Interval = 1000;
            this.timerSecondi.Tick += new System.EventHandler(this.timerSecondi_Tick);
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox7.BackgroundImage = global::Fifa2D.Properties.Resources.rete;
            this.pictureBox7.Location = new System.Drawing.Point(43, 287);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(39, 168);
            this.pictureBox7.TabIndex = 11;
            this.pictureBox7.TabStop = false;
            this.pictureBox7.Tag = "porta1";
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox5.BackgroundImage = global::Fifa2D.Properties.Resources.rete;
            this.pictureBox5.Location = new System.Drawing.Point(1174, 287);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(39, 168);
            this.pictureBox5.TabIndex = 10;
            this.pictureBox5.TabStop = false;
            this.pictureBox5.Tag = "porta2";
            // 
            // lblGiocatore1
            // 
            this.lblGiocatore1.BackColor = System.Drawing.Color.MediumBlue;
            this.lblGiocatore1.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGiocatore1.ForeColor = System.Drawing.Color.White;
            this.lblGiocatore1.Location = new System.Drawing.Point(88, 661);
            this.lblGiocatore1.Name = "lblGiocatore1";
            this.lblGiocatore1.Size = new System.Drawing.Size(130, 23);
            this.lblGiocatore1.TabIndex = 13;
            this.lblGiocatore1.Text = "GIOCATORE1";
            this.lblGiocatore1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblGiocatore2
            // 
            this.lblGiocatore2.BackColor = System.Drawing.Color.MediumBlue;
            this.lblGiocatore2.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGiocatore2.ForeColor = System.Drawing.Color.White;
            this.lblGiocatore2.Location = new System.Drawing.Point(1038, 661);
            this.lblGiocatore2.Name = "lblGiocatore2";
            this.lblGiocatore2.Size = new System.Drawing.Size(130, 23);
            this.lblGiocatore2.TabIndex = 14;
            this.lblGiocatore2.Text = "GIOCATORE2";
            this.lblGiocatore2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // timerMovimento
            // 
            this.timerMovimento.Interval = 25;
            this.timerMovimento.Tick += new System.EventHandler(this.timerMovimento_Tick);
            // 
            // timerPassaggio
            // 
            this.timerPassaggio.Tick += new System.EventHandler(this.timerPassaggio_Tick);
            // 
            // FrmPartita
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.ClientSize = new System.Drawing.Size(1264, 681);
            this.Controls.Add(this.lblGiocatore2);
            this.Controls.Add(this.lblGiocatore1);
            this.Controls.Add(this.pictureBox7);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pnlCampo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "FrmPartita";
            this.Text = "FIFA2D";
            this.Load += new System.EventHandler(this.FrmPartita_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmPartita_KeyDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.FrmPartita_KeyUp);
            this.pnlCampo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPalla)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCornerDxUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCornerDxDw)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCornerSxUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCornerSxDw)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbDwDx)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbDwSx)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbUpDx)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbUpSx)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbRigaCentrocampo)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbSquadra2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbSquadra1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlCampo;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblTimer;
        private System.Windows.Forms.Label lblRisultato;
        private System.Windows.Forms.PictureBox pbSquadra2;
        private System.Windows.Forms.PictureBox pbSquadra1;
        private System.Windows.Forms.Timer timerTempo;
        private System.Windows.Forms.Timer timerSecondi;
        private System.Windows.Forms.PictureBox pbDwDx;
        private System.Windows.Forms.PictureBox pbDwSx;
        private System.Windows.Forms.PictureBox pbUpDx;
        private System.Windows.Forms.PictureBox pbUpSx;
        private System.Windows.Forms.PictureBox pbCornerSxDw;
        private System.Windows.Forms.PictureBox pbCornerDxUp;
        private System.Windows.Forms.PictureBox pbCornerDxDw;
        private System.Windows.Forms.PictureBox pbCornerSxUp;
        private System.Windows.Forms.PictureBox pbRigaCentrocampo;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.Button btAvvia;
        private System.Windows.Forms.Label lblGiocatore1;
        private System.Windows.Forms.Label lblGiocatore2;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pbPalla;
        private System.Windows.Forms.Timer timerMovimento;
        private System.Windows.Forms.Timer timerPassaggio;
    }
}