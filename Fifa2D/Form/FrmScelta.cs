﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fifa2D
{
    public partial class FrmScelta : Form
    {
        public int _isquadra1 = 0, _isquadra2 = 1;

        bool _multiplayer=false;
        public FrmScelta(bool multiplayer)
        {
            InitializeComponent();
            _multiplayer = multiplayer;
        }

        private void btAvvia_Click(object sender, EventArgs e)
        {
            if (_isquadra1 != _isquadra2)
            {
                FrmPartita frmPartita = new FrmPartita(Program._squadre[_isquadra1], Program._squadre[_isquadra2]);
                DialogResult dr = frmPartita.ShowDialog(this);
                if (dr == DialogResult.Abort)
                {
                    this.DialogResult = DialogResult.Abort;
                    this.Close();
                }
            }
            else
            {
                MessageBox.Show("Non puoi far affrontare la stessa squadra");
            }
        }
        private void FrmScelta_Load(object sender, EventArgs e)
        {
            if (_multiplayer)
                lblGiocatore2.Text = "GIOCATORE 2";
            else
                lblGiocatore2.Text = "PC";
            PopolaLblTeams();

        }
        private void bt1Sx_Click(object sender, EventArgs e)
        {
            if (_isquadra1 > 0)
            {
                _isquadra1 -= 1;
            }
            else
                _isquadra1 = (Program._squadre.Length - 1);
            PopolaLblTeams();
        }

        private void bt1Dx_Click(object sender, EventArgs e)
        {
            if (_isquadra1 < 5)
            {
                _isquadra1 += 1;
            }
            else
                _isquadra1 = (0);
            PopolaLblTeams();
        }

        private void bt2Sx_Click(object sender, EventArgs e)
        {
            if (_isquadra2 > 0)
            {
                _isquadra2 -= 1;
            }
            else
                _isquadra2 = (Program._squadre.Length - 1);
            PopolaLblTeams();
        }

        private void bt2Dx_Click(object sender, EventArgs e)
        {
            if (_isquadra2 < 5)
            {
                _isquadra2 += 1;
            }
            else
                _isquadra2 = (0);
            PopolaLblTeams();
        }

        private void lblTeam2_Click(object sender, EventArgs e)
        {

        }

        private void lblTeam1_Click(object sender, EventArgs e)
        {

        }

        private void PopolaLblTeams()
        {
            string _nomeTeam1 = Program._squadre[_isquadra1].Nome;
            string _nomeTeam2 = Program._squadre[_isquadra2].Nome;

            lblTeam1.Text = _nomeTeam1;
            lblTeam2.Text = _nomeTeam2;

            pbTeam1.BackgroundImage = (Image)Properties.Resources.ResourceManager.GetObject(_nomeTeam1);
            pbTeam2.BackgroundImage = (Image)Properties.Resources.ResourceManager.GetObject(_nomeTeam2);
        }


    }
}
