﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Fifa2D
{
    public partial class FrmMain : Form
    {
        public FrmMain()
        {
            InitializeComponent();
        }
        string _nomeFile = "squadre.txt";
        private void btSinglePlayer_Click(object sender, EventArgs e)
        {
            FrmScelta frmScelta = new FrmScelta(false);
            DialogResult dr = frmScelta.ShowDialog(this);
        }

        private void btMultiPlayer_Click(object sender, EventArgs e)
        {
            FrmScelta frmScelta = new FrmScelta(true);
            DialogResult dr = frmScelta.ShowDialog(this);
        }
        private void FrmMain_Load(object sender, EventArgs e)
        {
            StreamReader sr = new StreamReader(_nomeFile);
            Program._squadre = new Program.sSquadra[0];
            string riga;
            int i = 0;
            while (!sr.EndOfStream)
            {
                riga = sr.ReadLine();
                string[] DatiSquadre = riga.Split('|');
                if (DatiSquadre.Length == 8)
                {
                    Array.Resize(ref Program._squadre, Program._squadre.Length + 1);
                    Program.sSquadra NuovaSquadra = new Program.sSquadra();
                    NuovaSquadra.Nome = DatiSquadre[0];
                    NuovaSquadra.Colore1 = (Program.eColori)Convert.ToInt16(DatiSquadre[1]);
                    NuovaSquadra.Colore2 = (Program.eColori)Convert.ToInt16(DatiSquadre[2]);
                    NuovaSquadra.POR = DatiSquadre[3];
                    NuovaSquadra.DC = DatiSquadre[4];
                    NuovaSquadra.CD = DatiSquadre[5];
                    NuovaSquadra.CS = DatiSquadre[6];
                    NuovaSquadra.ATT = DatiSquadre[7];
                    Program._squadre[i] = NuovaSquadra;
                    i++;
                }
            }
            sr.Close();
        }

        private void btTutorial_Click(object sender, EventArgs e)
        {
            FrmTutorial frmTutorial= new FrmTutorial();
            DialogResult dr = frmTutorial.ShowDialog(this);
        }
    }
}
