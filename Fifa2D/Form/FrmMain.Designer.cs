﻿namespace Fifa2D
{
    partial class FrmMain
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.btSinglePlayer = new System.Windows.Forms.Button();
            this.btMultiPlayer = new System.Windows.Forms.Button();
            this.btTutorial = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.pnlPulsanti = new System.Windows.Forms.Panel();
            this.pnlPulsanti.SuspendLayout();
            this.SuspendLayout();
            // 
            // btSinglePlayer
            // 
            this.btSinglePlayer.BackColor = System.Drawing.Color.Transparent;
            this.btSinglePlayer.BackgroundImage = global::Fifa2D.Properties.Resources._BtSingleplayer;
            this.btSinglePlayer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btSinglePlayer.Cursor = System.Windows.Forms.Cursors.Cross;
            this.btSinglePlayer.FlatAppearance.BorderSize = 0;
            this.btSinglePlayer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btSinglePlayer.Location = new System.Drawing.Point(311, 138);
            this.btSinglePlayer.Name = "btSinglePlayer";
            this.btSinglePlayer.Size = new System.Drawing.Size(303, 290);
            this.btSinglePlayer.TabIndex = 0;
            this.btSinglePlayer.UseVisualStyleBackColor = false;
            this.btSinglePlayer.Click += new System.EventHandler(this.btSinglePlayer_Click);
            // 
            // btMultiPlayer
            // 
            this.btMultiPlayer.BackColor = System.Drawing.Color.Transparent;
            this.btMultiPlayer.BackgroundImage = global::Fifa2D.Properties.Resources._BtMultiplayer;
            this.btMultiPlayer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btMultiPlayer.Cursor = System.Windows.Forms.Cursors.Cross;
            this.btMultiPlayer.FlatAppearance.BorderSize = 0;
            this.btMultiPlayer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btMultiPlayer.Location = new System.Drawing.Point(620, 138);
            this.btMultiPlayer.Name = "btMultiPlayer";
            this.btMultiPlayer.Size = new System.Drawing.Size(303, 290);
            this.btMultiPlayer.TabIndex = 1;
            this.btMultiPlayer.UseVisualStyleBackColor = false;
            this.btMultiPlayer.Click += new System.EventHandler(this.btMultiPlayer_Click);
            // 
            // btTutorial
            // 
            this.btTutorial.BackColor = System.Drawing.Color.Transparent;
            this.btTutorial.BackgroundImage = global::Fifa2D.Properties.Resources._BtIComeSiGiocaFifa2D;
            this.btTutorial.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btTutorial.Cursor = System.Windows.Forms.Cursors.Cross;
            this.btTutorial.FlatAppearance.BorderSize = 0;
            this.btTutorial.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btTutorial.Location = new System.Drawing.Point(311, 425);
            this.btTutorial.Name = "btTutorial";
            this.btTutorial.Size = new System.Drawing.Size(303, 189);
            this.btTutorial.TabIndex = 2;
            this.btTutorial.UseVisualStyleBackColor = false;
            this.btTutorial.Click += new System.EventHandler(this.btTutorial_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.Transparent;
            this.button4.BackgroundImage = global::Fifa2D.Properties.Resources._BtImpostazioniFifa2D;
            this.button4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button4.Cursor = System.Windows.Forms.Cursors.Cross;
            this.button4.FlatAppearance.BorderSize = 0;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Location = new System.Drawing.Point(620, 425);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(303, 189);
            this.button4.TabIndex = 3;
            this.button4.UseVisualStyleBackColor = false;
            // 
            // pnlPulsanti
            // 
            this.pnlPulsanti.AutoSize = true;
            this.pnlPulsanti.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pnlPulsanti.BackColor = System.Drawing.Color.Transparent;
            this.pnlPulsanti.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlPulsanti.Controls.Add(this.button4);
            this.pnlPulsanti.Controls.Add(this.btTutorial);
            this.pnlPulsanti.Controls.Add(this.btMultiPlayer);
            this.pnlPulsanti.Controls.Add(this.btSinglePlayer);
            this.pnlPulsanti.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlPulsanti.Location = new System.Drawing.Point(0, 0);
            this.pnlPulsanti.Name = "pnlPulsanti";
            this.pnlPulsanti.Size = new System.Drawing.Size(1264, 681);
            this.pnlPulsanti.TabIndex = 4;
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.BackgroundImage = global::Fifa2D.Properties.Resources._HomeFifa2D;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(1264, 681);
            this.Controls.Add(this.pnlPulsanti);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmMain";
            this.Text = "FIFA2D";
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.pnlPulsanti.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btSinglePlayer;
        private System.Windows.Forms.Button btMultiPlayer;
        private System.Windows.Forms.Button btTutorial;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Panel pnlPulsanti;
    }
}

