﻿namespace Fifa2D
{
    partial class FrmScelta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmScelta));
            this.btAvvia = new System.Windows.Forms.Button();
            this.btIndietro = new System.Windows.Forms.Button();
            this.bt1Dx = new System.Windows.Forms.Button();
            this.bt1Sx = new System.Windows.Forms.Button();
            this.bt2Sx = new System.Windows.Forms.Button();
            this.bt2Dx = new System.Windows.Forms.Button();
            this.lblGiocatore1 = new System.Windows.Forms.Label();
            this.lblGiocatore2 = new System.Windows.Forms.Label();
            this.lblTeam1 = new System.Windows.Forms.Label();
            this.lblTeam2 = new System.Windows.Forms.Label();
            this.pbTeam1 = new System.Windows.Forms.PictureBox();
            this.pbTeam2 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.pbTeam1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbTeam2)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btAvvia
            // 
            this.btAvvia.BackColor = System.Drawing.Color.Transparent;
            this.btAvvia.BackgroundImage = global::Fifa2D.Properties.Resources._BtAvvia;
            this.btAvvia.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btAvvia.Cursor = System.Windows.Forms.Cursors.Cross;
            this.btAvvia.FlatAppearance.BorderSize = 0;
            this.btAvvia.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btAvvia.Location = new System.Drawing.Point(386, 494);
            this.btAvvia.Name = "btAvvia";
            this.btAvvia.Size = new System.Drawing.Size(446, 182);
            this.btAvvia.TabIndex = 3;
            this.btAvvia.UseVisualStyleBackColor = false;
            this.btAvvia.Click += new System.EventHandler(this.btAvvia_Click);
            // 
            // btIndietro
            // 
            this.btIndietro.BackColor = System.Drawing.Color.Transparent;
            this.btIndietro.BackgroundImage = global::Fifa2D.Properties.Resources._BtIndietro;
            this.btIndietro.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btIndietro.Cursor = System.Windows.Forms.Cursors.Cross;
            this.btIndietro.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btIndietro.FlatAppearance.BorderSize = 0;
            this.btIndietro.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btIndietro.Location = new System.Drawing.Point(12, 512);
            this.btIndietro.Name = "btIndietro";
            this.btIndietro.Size = new System.Drawing.Size(152, 168);
            this.btIndietro.TabIndex = 4;
            this.btIndietro.UseVisualStyleBackColor = false;
            // 
            // bt1Dx
            // 
            this.bt1Dx.BackColor = System.Drawing.Color.Transparent;
            this.bt1Dx.BackgroundImage = global::Fifa2D.Properties.Resources._FrecciaDx;
            this.bt1Dx.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.bt1Dx.Cursor = System.Windows.Forms.Cursors.Cross;
            this.bt1Dx.FlatAppearance.BorderSize = 0;
            this.bt1Dx.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt1Dx.Location = new System.Drawing.Point(212, 285);
            this.bt1Dx.Name = "bt1Dx";
            this.bt1Dx.Size = new System.Drawing.Size(55, 61);
            this.bt1Dx.TabIndex = 5;
            this.bt1Dx.UseVisualStyleBackColor = false;
            this.bt1Dx.Click += new System.EventHandler(this.bt1Dx_Click);
            // 
            // bt1Sx
            // 
            this.bt1Sx.BackColor = System.Drawing.Color.Transparent;
            this.bt1Sx.BackgroundImage = global::Fifa2D.Properties.Resources._FrecciaSx;
            this.bt1Sx.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.bt1Sx.Cursor = System.Windows.Forms.Cursors.Cross;
            this.bt1Sx.FlatAppearance.BorderSize = 0;
            this.bt1Sx.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt1Sx.Location = new System.Drawing.Point(125, 285);
            this.bt1Sx.Name = "bt1Sx";
            this.bt1Sx.Size = new System.Drawing.Size(55, 61);
            this.bt1Sx.TabIndex = 8;
            this.bt1Sx.UseVisualStyleBackColor = false;
            this.bt1Sx.Click += new System.EventHandler(this.bt1Sx_Click);
            // 
            // bt2Sx
            // 
            this.bt2Sx.BackColor = System.Drawing.Color.Transparent;
            this.bt2Sx.BackgroundImage = global::Fifa2D.Properties.Resources._FrecciaSx;
            this.bt2Sx.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.bt2Sx.Cursor = System.Windows.Forms.Cursors.Cross;
            this.bt2Sx.FlatAppearance.BorderSize = 0;
            this.bt2Sx.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt2Sx.Location = new System.Drawing.Point(119, 285);
            this.bt2Sx.Name = "bt2Sx";
            this.bt2Sx.Size = new System.Drawing.Size(55, 61);
            this.bt2Sx.TabIndex = 10;
            this.bt2Sx.UseVisualStyleBackColor = false;
            this.bt2Sx.Click += new System.EventHandler(this.bt2Sx_Click);
            // 
            // bt2Dx
            // 
            this.bt2Dx.BackColor = System.Drawing.Color.Transparent;
            this.bt2Dx.BackgroundImage = global::Fifa2D.Properties.Resources._FrecciaDx;
            this.bt2Dx.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.bt2Dx.Cursor = System.Windows.Forms.Cursors.Cross;
            this.bt2Dx.FlatAppearance.BorderSize = 0;
            this.bt2Dx.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt2Dx.Location = new System.Drawing.Point(206, 285);
            this.bt2Dx.Name = "bt2Dx";
            this.bt2Dx.Size = new System.Drawing.Size(55, 61);
            this.bt2Dx.TabIndex = 9;
            this.bt2Dx.UseVisualStyleBackColor = false;
            this.bt2Dx.Click += new System.EventHandler(this.bt2Dx_Click);
            // 
            // lblGiocatore1
            // 
            this.lblGiocatore1.BackColor = System.Drawing.Color.Transparent;
            this.lblGiocatore1.Font = new System.Drawing.Font("Microsoft JhengHei UI", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGiocatore1.ForeColor = System.Drawing.Color.White;
            this.lblGiocatore1.Location = new System.Drawing.Point(73, 372);
            this.lblGiocatore1.Name = "lblGiocatore1";
            this.lblGiocatore1.Size = new System.Drawing.Size(249, 37);
            this.lblGiocatore1.TabIndex = 11;
            this.lblGiocatore1.Text = "GIOCATORE 1";
            this.lblGiocatore1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblGiocatore2
            // 
            this.lblGiocatore2.BackColor = System.Drawing.Color.Transparent;
            this.lblGiocatore2.Font = new System.Drawing.Font("Microsoft JhengHei UI", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGiocatore2.ForeColor = System.Drawing.Color.White;
            this.lblGiocatore2.Location = new System.Drawing.Point(70, 372);
            this.lblGiocatore2.Name = "lblGiocatore2";
            this.lblGiocatore2.Size = new System.Drawing.Size(249, 37);
            this.lblGiocatore2.TabIndex = 12;
            this.lblGiocatore2.Text = "GIOCATORE 2";
            this.lblGiocatore2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblTeam1
            // 
            this.lblTeam1.BackColor = System.Drawing.Color.Transparent;
            this.lblTeam1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblTeam1.Font = new System.Drawing.Font("Verdana", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTeam1.ForeColor = System.Drawing.Color.White;
            this.lblTeam1.Location = new System.Drawing.Point(70, 55);
            this.lblTeam1.Name = "lblTeam1";
            this.lblTeam1.Size = new System.Drawing.Size(252, 37);
            this.lblTeam1.TabIndex = 13;
            this.lblTeam1.Text = "TEAM1";
            this.lblTeam1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblTeam1.Click += new System.EventHandler(this.lblTeam1_Click);
            // 
            // lblTeam2
            // 
            this.lblTeam2.BackColor = System.Drawing.Color.Transparent;
            this.lblTeam2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblTeam2.Font = new System.Drawing.Font("Verdana", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTeam2.ForeColor = System.Drawing.Color.White;
            this.lblTeam2.Location = new System.Drawing.Point(62, 55);
            this.lblTeam2.Name = "lblTeam2";
            this.lblTeam2.Size = new System.Drawing.Size(257, 37);
            this.lblTeam2.TabIndex = 14;
            this.lblTeam2.Text = "TEAM2";
            this.lblTeam2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblTeam2.Click += new System.EventHandler(this.lblTeam2_Click);
            // 
            // pbTeam1
            // 
            this.pbTeam1.BackColor = System.Drawing.Color.Transparent;
            this.pbTeam1.BackgroundImage = global::Fifa2D.Properties.Resources.NAPOLI;
            this.pbTeam1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pbTeam1.Enabled = false;
            this.pbTeam1.ImageLocation = "";
            this.pbTeam1.Location = new System.Drawing.Point(113, 98);
            this.pbTeam1.Name = "pbTeam1";
            this.pbTeam1.Size = new System.Drawing.Size(170, 166);
            this.pbTeam1.TabIndex = 15;
            this.pbTeam1.TabStop = false;
            // 
            // pbTeam2
            // 
            this.pbTeam2.BackColor = System.Drawing.Color.Transparent;
            this.pbTeam2.BackgroundImage = global::Fifa2D.Properties.Resources.JUVENTUS;
            this.pbTeam2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pbTeam2.Location = new System.Drawing.Point(105, 98);
            this.pbTeam2.Name = "pbTeam2";
            this.pbTeam2.Size = new System.Drawing.Size(170, 166);
            this.pbTeam2.TabIndex = 16;
            this.pbTeam2.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::Fifa2D.Properties.Resources._RettangoloSquadre;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel1.Controls.Add(this.pbTeam1);
            this.panel1.Controls.Add(this.bt1Dx);
            this.panel1.Controls.Add(this.bt1Sx);
            this.panel1.Controls.Add(this.lblGiocatore1);
            this.panel1.Controls.Add(this.lblTeam1);
            this.panel1.Location = new System.Drawing.Point(213, 99);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(390, 411);
            this.panel1.TabIndex = 17;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.BackgroundImage = global::Fifa2D.Properties.Resources._RettangoloSquadre;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel2.Controls.Add(this.lblTeam2);
            this.panel2.Controls.Add(this.bt2Dx);
            this.panel2.Controls.Add(this.pbTeam2);
            this.panel2.Controls.Add(this.bt2Sx);
            this.panel2.Controls.Add(this.lblGiocatore2);
            this.panel2.Location = new System.Drawing.Point(602, 99);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(390, 411);
            this.panel2.TabIndex = 18;
            // 
            // FrmScelta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.BackgroundImage = global::Fifa2D.Properties.Resources._HubSceltaFifa2D;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(1264, 681);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btIndietro);
            this.Controls.Add(this.btAvvia);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmScelta";
            this.Text = "FIFA2D";
            this.Load += new System.EventHandler(this.FrmScelta_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbTeam1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbTeam2)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btAvvia;
        private System.Windows.Forms.Button btIndietro;
        private System.Windows.Forms.Button bt1Dx;
        private System.Windows.Forms.Button bt1Sx;
        private System.Windows.Forms.Button bt2Sx;
        private System.Windows.Forms.Button bt2Dx;
        private System.Windows.Forms.Label lblGiocatore1;
        private System.Windows.Forms.Label lblGiocatore2;
        private System.Windows.Forms.Label lblTeam1;
        private System.Windows.Forms.Label lblTeam2;
        private System.Windows.Forms.PictureBox pbTeam1;
        private System.Windows.Forms.PictureBox pbTeam2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
    }
}