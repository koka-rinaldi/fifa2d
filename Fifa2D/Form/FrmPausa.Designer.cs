﻿namespace Fifa2D
{
    partial class FrmPausa
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btRiprendi = new System.Windows.Forms.Button();
            this.btEsci = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btRiprendi
            // 
            this.btRiprendi.BackColor = System.Drawing.Color.Transparent;
            this.btRiprendi.BackgroundImage = global::Fifa2D.Properties.Resources._BtRiprendi;
            this.btRiprendi.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btRiprendi.Cursor = System.Windows.Forms.Cursors.Cross;
            this.btRiprendi.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btRiprendi.FlatAppearance.BorderSize = 0;
            this.btRiprendi.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btRiprendi.Location = new System.Drawing.Point(105, 51);
            this.btRiprendi.Name = "btRiprendi";
            this.btRiprendi.Size = new System.Drawing.Size(351, 399);
            this.btRiprendi.TabIndex = 1;
            this.btRiprendi.UseVisualStyleBackColor = false;
            // 
            // btEsci
            // 
            this.btEsci.BackColor = System.Drawing.Color.Transparent;
            this.btEsci.BackgroundImage = global::Fifa2D.Properties.Resources._BtEsci;
            this.btEsci.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btEsci.Cursor = System.Windows.Forms.Cursors.Cross;
            this.btEsci.DialogResult = System.Windows.Forms.DialogResult.Abort;
            this.btEsci.FlatAppearance.BorderSize = 0;
            this.btEsci.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btEsci.Location = new System.Drawing.Point(462, 51);
            this.btEsci.Name = "btEsci";
            this.btEsci.Size = new System.Drawing.Size(351, 399);
            this.btEsci.TabIndex = 2;
            this.btEsci.UseVisualStyleBackColor = false;
            this.btEsci.Click += new System.EventHandler(this.btEsci_Click);
            // 
            // FrmPausa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.BackgroundImage = global::Fifa2D.Properties.Resources._HomePausa;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(921, 553);
            this.Controls.Add(this.btRiprendi);
            this.Controls.Add(this.btEsci);
            this.Name = "FrmPausa";
            this.Text = "FIFA2D";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmPausa_FormClosing);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btRiprendi;
        private System.Windows.Forms.Button btEsci;
    }
}