﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fifa2D
{
    public partial class FrmPausa : Form
    {
        public FrmPausa()
        {
            InitializeComponent();
        }

        private void FrmPausa_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.DialogResult != DialogResult.Abort)
            {
                this.DialogResult = DialogResult.OK;
            }
        }


        private void btEsci_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Abort;
        }
    }
}
